
/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
import com.mycompany.softwaredevelopment2022.OX_V2;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Pinkz7_
 */
public class OXProgramTest {

    public OXProgramTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCheckVerticlePlayerOCol1Win() {
        char table[][] = {{'O', '-', '-'},
                               {'O', '-', '-'},
                               {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 1;
        assertEquals(true, OX_V2.checkVertical(table, currentPlayer, col));
    }

    @Test
    public void testCheckVerticlePlayerOCol2Win() {
        char table[][] = {{'-', 'O', '-'},
                               {'-', 'O', '-'},
                               {'-', 'O', '-'}};
        char currentPlayer = 'O';
        int col = 2;
         assertEquals(true, OX_V2.checkVertical(table, currentPlayer, col));
    }
    
    @Test
        public void testCheckVerticlePlayerOCol3Win() {
        char table[][] = {{'-', '-', 'O'},
                               {'-', '-', 'O'},
                               {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 3;
        assertEquals(true, OX_V2.checkVertical(table, currentPlayer, col));
    }
      
     @Test
        public void testCheckHorizontalPlayerORow1Win() {
        char table[][] = {{'O', 'O', 'O'},
                               {'-', '-', '-'},
                               {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 1;
        assertEquals(true, OX_V2.checkHorizontal(table, currentPlayer, row));
    }
        
     @Test
        public void testCheckHorizontalPlayerORow2Win() {
        char table[][] = {{'-', '-', '-'},
                               {'O', 'O', 'O'},
                               {'-', '-', '-'}};
        char currentPlayer = 'O';
        int row = 2;
        assertEquals(true, OX_V2.checkHorizontal(table, currentPlayer, row));
    }
        
     @Test
        public void testCheckHorizontalPlayerORow3Win() {
        char table[][] = {{'-', '-', '-'},
                               {'-', '-', '-'},
                               {'O', 'O', 'O'}};
        char currentPlayer = 'O';
        int row = 3;
        assertEquals(true, OX_V2.checkHorizontal(table, currentPlayer, row));
    }
        
     @Test
        public void testcheckXPlayerOCol1toRow3Win() {
        char table[][] = {{'O', '-', '-'},
                               {'-', 'O', '-'},
                               {'-', '-', 'O'}};
        char currentPlayer = 'O';
        int col = 1;
        int row = 3;
        assertEquals(true, OX_V2.checkX(table, currentPlayer));
    }
        
     @Test
        public void testcheckXPlayerOCol3toRow1Win() {
        char table[][] = {{'-', '-', 'O'},
                               {'-', 'O', '-'},
                               {'O', '-', '-'}};
        char currentPlayer = 'O';
        int col = 3;
        int row = 1;
        assertEquals(true, OX_V2.checkX(table, currentPlayer));
    }
          
    @Test
    public void testCheckVerticlePlayerXCol1Win() {
        char table[][] = {{'X', '-', '-'},
                               {'X', '-', '-'},
                               {'X', '-', '-'}};
        char currentPlayer = 'X';
        int col = 1;
        assertEquals(true, OX_V2.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticlePlayerXCol2Win() {
        char table[][] = {{'-', 'X', '-'},
                               {'-', 'X', '-'},
                               {'-', 'X', '-'}};
        char currentPlayer = 'X';
        int col = 2;
        assertEquals(true, OX_V2.checkVertical(table, currentPlayer, col));
    }
    
    @Test
    public void testCheckVerticlePlayerXCol3Win() {
        char table[][] = {{'-', '-', 'X'},
                               {'-', '-', 'X'},
                               {'-', '-', 'X'}};
        char currentPlayer = 'X';
        int col = 3;
        assertEquals(true, OX_V2.checkVertical(table, currentPlayer, col));
    }
    
    @Test
        public void testcheckDraw() {
        int count = 9;
        assertEquals(true, OX_V2.checkDraw(count));
    }
}
